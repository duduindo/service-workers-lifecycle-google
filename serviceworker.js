

self.addEventListener('install', event => {
	console.log('V1 installing...');

	// cache a cat SVG
	event.waitUntil(
		caches.open('static-v1').then(cache => cache.add('/world-replace.svg'))
	);
});


self.addEventListener('activate', event => {
	console.log('V1 now ready to handle fetches!');
});


self.addEventListener('fetch', event => {
	const url = new URL(event.request.url);

	// Serve the world SVG from the cache if the request is
	// same-origiin and the path is '/world.svg'
	if (url.origin == location.origin && url.pathname == '/world.svg') {
		console.log('world SVG');
		event.respondWith(caches.match('/world-replace.svg'));
	}
});